<<<<<<< HEAD
Anotación: Escribir un README.md en el directorio raíz de nuestro repositorio explicando de qué se trata el curso. Esto debe hacerse en la rama principal:


Agregar una apartado al README.md con links y descripciones de al menos 2 herramientas web para aprender ramas en Git. En la rama principal.

<a name="top"></a>
# Curso de GIT en el IPAP
 
Anotación: Escribir un README.md en el directorio raíz de nuestro repositorio explicando de qué se trata el curso. Esto debe hacerse en la rama principal:


Agregar una apartado al README.md con links y descripciones de al menos 2 herramientas web para aprender ramas en Git. En la rama principal.

<a name="top"></a>
# Curso de GIT en el IPAP
 
## Índice de contenidos
* [Herramienta 1](#item1)
* [Herramienta 2](#item2)

<a name="item1"></a>
### Contenido 1
 
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 
[Subir](#top)
 
<a name="item2"></a>
### Contenido 2
 
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
* [Herramienta 1](#item1)
* [Herramienta 2](#item2)

<a name="item1"></a>
### Contenido 1
 
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 
[Subir](#top)
 
<a name="item2"></a>
### Contenido 2
 
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
 
[Subir](#top)
=======
![Poncho](img/poncho.gif)

# Poncho

Base de html y css para la creación de sitios pertenecientes a la Administración Pública Nacional de la República Argentina.

En este repositorio podés descargar los archivos de Poncho para trabajar de manera local.
Esta nueva versión de Poncho incluye cambios de colores y otros elementos que mejoran cuestiones de accesibilidad.

Para usar Poncho en un sitio, ver [la documentación](http://argob.github.io/poncho).  
También estamos en [NPM](https://www.npmjs.com/package/ar-poncho).

## Índice de contenidos
* [Apartado por Curso IPSP ](#cursoIpap)

## ¿Cómo instalar Poncho?

#### Si usás NPM

* Ejecutá en la consola el comando **npm i ar-poncho**

#### Si descargás los archivos manualmente

* También podés [descargar las plantillas de Poncho](http://argob.github.io/poncho/plantillas/paginas-de-argentina/) o crear archivos html nuevos, asegurándote de que en los html estén declaradas estas dependencias:
  
#### Fuente Tipográfica - Encode

* ``` <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Encode+Sans:wght@100;200;300;400;500;600;700;800;900&amp;display=swap" media="all" /> ```

#### CSS - Bootstrap (v-3.4.1), Font Awesome (v-4.7.0), Poncho, Iconos

* ``` <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"  rel="stylesheet"> ```
* ``` <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> ```
* ``` <link rel="stylesheet" href="dist/css/poncho.min.css"> ```
* ``` <link rel="stylesheet" href="dist/css/icono-arg.css"> ```

#### JavaScript -  Jquery, Bootstrap (v-3.4.1)

* ``` <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> ```
* ``` <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js”></script> ```


<a name="cursoIpap"></a>
### cursoIpap
 
Agregar un apartado al README.md donde explique de qué se trata el repositorio clonado del Estado Argentino. Esto debe hacerse en la rama proyectos 
 
[Subir](#top)
  
>>>>>>> proyectos
